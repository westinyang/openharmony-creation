## 开源项目

- [⭐F-OH](https://gitee.com/westinyang/f-oh) `OpenHarmony自由开源软件的应用中心`
- [🔍HapViewer](https://gitee.com/westinyang/hap-viewer) `一个跨平台的hap查看器，方便开发者在电脑上预览hap的信息`
- [🔍HapViewerAndroid](https://gitee.com/westinyang/hap-viewer-android) `安卓版的hap查看器`
- [📱DeviceInfo](https://gitee.com/westinyang/device-info) `设备信息查看`
- [🌐BrowserCE](https://gitee.com/westinyang/browser-ce) `浏览器社区版，基于官方demo定制增强`
- [🔋PowerMGT](https://gitee.com/westinyang/power-mgt) `电源管理，电源键拯救者`
- [🎮Tetris](https://gitee.com/westinyang/tetris) `俄罗斯方块，掌机经典`
- [⌨VirtualKeyboard](https://gitee.com/westinyang/virtual-keyboard) `支持中文输入的虚拟键盘应用`
- [🚀WebApp](https://gitee.com/westinyang/web-app) `网页封装样例`

## 其他项目

- [OHScrcpy](https://www.bilibili.com/read/cv24125018) `OpenHarmony投屏工具`
- OpenHarmony壁纸设置+已修复的启动器整合包
    - [3.2 Release (phone)](https://www.bilibili.com/read/cv23658602)
    - [4.0 Release (phone, pad)](https://www.bilibili.com/read/cv28611152)
- [OpenHarmony实验性的终端应用](https://www.bilibili.com/read/cv24282525)
- [OpenHarmony开发者论坛网页增强插件](https://gitee.com/westinyang/codelabs/blob/master/userscript/OpenHarmonyDeveloperForumPlus.user.js)

## 精选视频

- 2023-02-27 ✨创建OpenHarmony开发者组织：[OHOS Dev](https://gitee.com/ohos-dev) `为OpenHarmony的未来而创建，Peace & Love`
- 2023-03-17 [**🟢基于开源移植仓库编译“一加6T”的OpenHarmony固件，并分享发布~**](https://www.bilibili.com/video/BV12P411Z7HB) `首次B站投稿，开启创作之路`
- 2023-03-19 [🟢OpenHarmony开源应用第一弹：设备信息](https://www.bilibili.com/video/BV1Zb411Z72i)
- 2023-04-11 [**🟢历时七天，从零开发，F-OH（OpenHarmony首个自由开源软件的应用中心）诞生开源~**]()
- 2023-04-20 [🏆UP主里程碑：粉丝数破1000](https://www.bilibili.com/opus/786333410331197513)
- 2023-05-04 [**🟢首次催更，码力全开，HapViewer（OpenHarmony首个跨平台的HAP查看器）诞生开源~**](https://www.bilibili.com/video/BV1HX4y127ub)
- 2023-05-08 [🔵OpenHarmony3.2壁纸设置，和海洋之心说再见](https://www.bilibili.com/video/BV1bP41117Lz)
- 2023-05-23 [🟢2023 Q2 OpenHarmony精选开源应用“串烧”](https://www.bilibili.com/video/BV1Qs4y1z7ED)
- 2023-06-03 [**🔵爆肝一周，得意之作，OHScrcpy（OpenHarmony首个投屏工具）横空出世~**](https://www.bilibili.com/video/BV1xz4y1q7S1)
- 2023-06-17 [🟢HapViewer for Android（HAP查看器的续作安卓版）诞生开源~](https://www.bilibili.com/video/BV1pX4y147V8)
- 2023-07-23 [💻“开鸿教学2.0”系列视频第一期：OpenHarmony截屏的5种方式](https://www.bilibili.com/video/BV1ju4y1U7uk)
- 2023-08-19 [📱武夷飞鸿SEEK100开发板，商用发行版FlyHongOS使用体验](https://www.bilibili.com/video/BV1fh4y1S7Ac)
- 2023-08-20 [🏆UP主里程碑：累计播放量破50万](https://www.bilibili.com/opus/786333410331197513)
- 2023-08-30 [📱PolyOS Mobile（OpenHarmomy 3.2）模拟器使用体验](https://www.bilibili.com/video/BV1mu411A7Wh)
- 2023-09-06 [**🔥一眼半年，全程高能，OpenHarmony热血创作混剪~**](https://www.bilibili.com/video/BV1p14y1C7T4)
- 2023-10-08 [🎖️获得OpenHarmony共建者徽章，编号：005996，翻译：我996😂](https://www.bilibili.com/opus/849961057083457576)
- 2023-10-13 [🎬大型纪录片《开源鸿蒙生态发展》之2023](https://www.bilibili.com/video/BV1LC4y157Jc)
- 2023-11-10 [📱开源鸿蒙PC Orange Pi OS (OH) 使用体验](https://www.bilibili.com/video/BV1q84y1Q7Gz)
- 2023-11-26 [**🔥正式更名开鸿派，OpenHarmony开源组织 OHOS Dev 的成长之路**](https://www.bilibili.com/video/BV1Ma4y1f7MD)
- 2023-11-29 [🔒终端安全，初步探索，无源码修改HAP探讨测试](https://www.bilibili.com/video/BV18C4y1y7xM)
- 2023-12-12 [📱OpenHarmony4.0平板使用体验](https://www.bilibili.com/video/BV1gH4y1C7dg)
- 2023-12-17 [🔵OpenHarmony4.0壁纸设置，秒变遥遥领先](https://www.bilibili.com/video/BV1yC4y1Q7YY)
- 2023-12-27 [🏆UP主里程碑：累计播放量破100万](https://www.bilibili.com/opus/879660128879509576)
- 2023-12-27 [**🔥五大OpenHarmony应用商店，鸿蒙原生开/闭源应用共多少？**](https://www.bilibili.com/video/BV1rC4y1K7JD)
- 2023-12-30 [**⚫F-OH开源应用中心停服公告**](https://www.bilibili.com/video/BV1KT4y1x7Qh)